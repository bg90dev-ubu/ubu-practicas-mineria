<div align="center">

# Minería de Datos UBU

[![Project](https://img.shields.io/badge/Project-University-blueviolet.svg)][repo-link]
[![Repository](https://img.shields.io/badge/gitlab-purple?logo=gitlab)][repo-link]
[![Language](https://img.shields.io/badge/Java-00718B?logo=java)][java-link]

Prácticas y ejercicios de la asignatura de Minería de datos cursada en la Universidad de Burgos

</div>
<hr>

## Built with

### Technologies

[<img src="https://avatars.githubusercontent.com/u/16873035?s=200&v=4" width=50 alt="Scilab">][scilab-link]
[<img src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/java/java.png" width=50 alt="Java">][java-link]

### Platforms

[<img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/eclipse.png" width=50 alt="Eclipse">][eclipse-link]

<div align="center">

## Authors

### **Borja Gete**

[![Mail](https://img.shields.io/badge/borjag90dev@gmail.com-DDDDDD?style=for-the-badge&logo=gmail)][borjag90dev-gmail]
[![Github](https://img.shields.io/badge/BorjaG90-000000.svg?&style=for-the-badge&logo=github&logoColor=white)][borjag90dev-github]
[![Gitlab](https://img.shields.io/badge/BorjaG90-purple.svg?&style=for-the-badge&logo=gitlab)][borjag90dev-gitlab]
[![LinkedIn](https://img.shields.io/badge/borjag90-0077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white)][borjag90dev-linkedin]

### **Plamen Petkov**

[![Mail](https://img.shields.io/badge/petkov092@gmail.com-DDDDDD?style=for-the-badge&logo=gmail)][plamen-gmail]
[![BitBucket](https://img.shields.io/badge/Plamen_Petkov-0747A6.svg?&style=for-the-badge&logo=bitbucket)][plamen-bitbucket]
[![LinkedIn](https://img.shields.io/badge/Plamen_Petkov-0077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white)][plamen-linkedin]


</div>

[borjag90dev-gmail]: mailto:borjag90dev@gmail.com
[borjag90dev-github]: https://github.com/BorjaG90
[borjag90dev-gitlab]: https://gitlab.com/BorjaG90
[borjag90dev-linkedin]: https://www.linkedin.com/in/borjag90/
[plamen-gmail]: mailto:petkov092@gmail.com
[plamen-linkedin]: https://www.linkedin.com/in/plamen-petkov/
[plamen-bitbucket]: https://bitbucket.org/ppp0015/
[repo-link]: https://gitlab.com/bg90dev-ubu/ubu-practicas-mineria
[eclipse-link]: https://www.eclipse.org/downloads/
[scilab-link]: https://www.scilab.org/
[java-link]: https://www.java.com/es/
